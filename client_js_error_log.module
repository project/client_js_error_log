<?php

/**
 * @file
 * Client side JavaScript error logging module.
 *
 * This module logs those JavaScript errors which happen on client side,
 * so helps the development.
 */

module_load_include('inc', 'client_js_error_log', 'includes/config');

/**
 * Implements hook_help().
 *
 * @see hook_help()
 */
function client_js_error_log_help($path, $arg) {
  switch ($path) {
    case 'admin/help#client_js_error_log':
      $filepath = drupal_dirname(__FILE__) . '/README.txt';
      if (file_exists($filepath)) {
        $readme = file_get_contents($filepath);
      }

      if (!isset($readme)) {
        return NULL;
      }
      if (module_exists('markdown')) {
        $filters = module_invoke('markdown', 'filter_info');
        $info = $filters['filter_markdown'];

        if (function_exists($info['process callback'])) {
          $output = $info['process callback']($readme, NULL);
        }
        else {
          $output = '<pre>' . $readme . '</pre>';
        }
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }

      return $output;
  }
}

/**
 * Implements hook_permission().
 */
function client_js_error_log_permission() {
  return array(
    'administer client js error log' => array(
      'title' => t('Administer Client JS error log'),
      'description' => t('Administer Client JS error log settings.'),
    ),
    'view client js error log' => array(
      'title' => t('View Client JS error log'),
      'description' => t('View Client JS error log records.'),
    ),
  );
}

/**
 * Implements hook_menu().
 *
 * @see hook_menu()
 */
function client_js_error_log_menu() {
  $items = array();

  // Register the log save path.
  $items[CLIENT_JS_ERROR_LOG_URL] = array(
    'page callback' => 'client_js_error_log_error',
    // It is public to allow the system also logging anonymous users.
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  // Register the configuration form path.
  $items['admin/config/system/client-js-error'] = array(
    'title' => 'Client side JS error log configuration',
    'description' => 'Record client side JS errors on your site',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('client_js_error_log_admin_settings_form'),
    'access arguments' => array('administer client js error log'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/client_js_error_log.admin.inc',
  );

  // Register the log lister page.
  $items['admin/reports/client-js-error'] = array(
    'title' => 'Client js error log messages',
    'description' => 'View client side js errors that have recently been logged.',
    'page callback' => 'client_js_error_log_overview',
    'access arguments' => array('view client js error log'),
    'file' => 'includes/client_js_error_log.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_page_build().
 *
 * @see hook_page_build()
 */
function client_js_error_log_page_build(&$page) {

  // Do not include on ajax request.
  if (isset($_SERVER['HTTP_X_REQUESTED_WITH'])
    && drupal_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'
  ) {
    return FALSE;
  }

  global $base_url;

  $log_max_count = (int) variable_get('client_js_error_log_max_count',
    DEFAULT_CLIENT_JS_ERROR_LOG_MAX_COUNT);

  $log_url = $base_url . '/' . ltrim(CLIENT_JS_ERROR_LOG_URL, '/');

  $page['header']['#attached']['js'][] = array(
    // This JS should be on the most top position to catch the most errors.
    'weight' => '-99',
    'type' => 'file',
    'data' => drupal_get_path('module', 'client_js_error_log')
      . '/js/client_js_error_log.js',
  );
  $page['header']['#attached']['js'][] = array(
    // Instead of Drupal.settings we use inline code to transfer parameters to
    // JS, because Drupal setting is available only after document ready,
    // but so we can catch errors earlier.
    'type' => 'inline',
    // It should be after the external JS include.
    'weight' => '-98',
    'data' => "if('clientJsErrorLog' in Drupal){Drupal.clientJsErrorLog.setLogMaxCount({$log_max_count}).setLogUrl('{$log_url}');}",
  );
}

/**
 * Saves the log record to database.
 */
function client_js_error_log_error() {
  global $user;
  $error = isset($_POST['error']) ? (array) $_POST['error'] : array();

  $message = isset($error['message']) ? $error['message'] : '';
  $url = isset($error['url']) ? $error['url'] : '';
  $line = isset($error['line']) ? (int) $error['line'] : 0;
  $column = isset($error['column']) ? (int) $error['column'] : 0;
  $stack = isset($error['stack']) ? $error['stack'] : '';
  $referrer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
  $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? trim($_SERVER['HTTP_USER_AGENT']) : '';

  $data = array(
    'user_id' => $user->uid,
    'message' => $message,
    'url' => $url,
    'line' => $line,
    'col_number' => $column,
    'stack' => $stack,
    'referrer' => $referrer,
    'user_agent' => $user_agent,
    'ip_address' => ip_address(),
    'create_date' => date('Y-m-d H:i:s'),
  );
  client_js_error_log_insert_error($data);

  drupal_json_output();
}

/**
 * Inserts the log record to database.
 *
 * @param array $data
 *   Data array with error details.
 *
 * @throws \Exception
 */
function client_js_error_log_insert_error($data) {

  // Checks how many records are registered in the last x minute from the IP.
  $interval_minute = variable_get('client_js_error_log_interval_minute', DEFAULT_CLIENT_JS_ERROR_LOG_INTERVAL_MINUTE);
  $query = db_select(TABLE_CLIENT_JS_ERROR_LOG, 'log')
    ->condition('log.ip_address', $data['ip_address'], '=')
    ->fields('log', array('id'))
    ->where('log.create_date > DATE_SUB(NOW(), INTERVAL :interval_minute MINUTE)',
      array(':interval_minute' => $interval_minute))
    ->execute();
  $log_count = $query->rowCount();
  $log_max_count = variable_get('client_js_error_log_max_in_interval', DEFAULT_CLIENT_JS_ERROR_LOG_MAX_IN_INTERVAL);

  if ($log_max_count > $log_count) {
    db_insert(TABLE_CLIENT_JS_ERROR_LOG)
      ->fields(array(
        'user_id' => $data['user_id'],
        'message' => $data['message'],
        'url' => $data['url'],
        'line' => $data['line'],
        'col_number' => $data['col_number'],
        'stack' => $data['stack'],
        'referrer' => $data['referrer'],
        'user_agent' => $data['user_agent'],
        'ip_address' => $data['ip_address'],
        'create_date' => $data['create_date'],
      ))->execute();
  }
}

/**
 * Implements hook_cron().
 */
function client_js_error_log_cron() {

  // Remove old records in every 8 hour.
  $interval = 60 * 60 * 8;
  if (REQUEST_TIME
    >= variable_get('client_js_error_log_delete_record_next_execution', 0)
  ) {
    client_js_error_log_cleanup_table();
    variable_set('client_js_error_log_delete_record_next_execution',
      REQUEST_TIME + $interval);
  }

  // Send aggregated log records in email in every x hour.
  $interval_hour = variable_get('client_js_error_log_send_email_interval_hour',
    DEFAULT_CLIENT_JS_ERROR_LOG_SEND_EMAIL_INTERVAL_HOUR);
  if ($interval_hour > 0) {
    if (REQUEST_TIME
      >= variable_get('client_js_error_log_send_email_next_execution', 0)
    ) {
      client_js_error_log_send_batch_email($interval_hour);
      variable_set('client_js_error_log_send_email_next_execution',
        REQUEST_TIME + (60 * 60 * $interval_hour));
    }
  }
}

/**
 * Cleanups log table with remove old records.
 */
function client_js_error_log_cleanup_table() {
  $interval_day = variable_get('client_js_error_log_delete_record_interval_day',
    DEFAULT_CLIENT_JS_ERROR_LOG_DELETE_RECORD_INTERVAL_DAY);
  if ($interval_day > 0) {
    // Delete all table entries older than the $interval_day days.
    db_delete(TABLE_CLIENT_JS_ERROR_LOG)
      ->where('create_date < DATE_SUB(NOW(), INTERVAL :interval_day DAY)',
        array(':interval_day' => $interval_day))
      ->execute();
  }
}

/**
 * Sends email with summary of client js error log records.
 *
 * @param int $interval_hour
 *   Interval in hours.
 *
 * @return bool
 *   True on success otherwise false.
 */
function client_js_error_log_send_batch_email($interval_hour) {

  $email_addresses = client_js_error_log_separate_email_address(
    variable_get('client_js_error_log_mail_address', ''));
  if (empty($email_addresses)) {
    return FALSE;
  }

  $result = db_select(TABLE_CLIENT_JS_ERROR_LOG, 'log')
    ->distinct()
    ->fields('log', array('message'))
    ->where('log.create_date > DATE_SUB(NOW(), INTERVAL :interval_hour HOUR)',
      array(':interval_hour' => $interval_hour))
    ->range(0, 50)
    ->execute();

  if (!empty($result)) {
    $params = array();
    $params['subject'] = t('[@site_name] Client Js error summary', array(
      '@site_name' => variable_get('site_name', 'Drupal'),
    ));
    $params['result'] = $result;
    $params['to'] = implode(', ', $email_addresses);

    $sent = drupal_mail('client_js_error_log',
      'client_js_error_log_send_aggregated_log_email',
      '',
      language_default(),
      $params);

    return (bool) $sent['result'];
  }

  return FALSE;
}

/**
 * Implements hook_mail().
 *
 * @see hook_mail()
 */
function client_js_error_log_mail($key, &$message, $params) {
  global $base_url;

  switch ($key) {
    case 'client_js_error_log_send_aggregated_log_email':
      $message['subject'] = $params['subject'];
      $message['body'][] = t("Site:@base_url", array('@base_url' => $base_url));
      $message['body'][] = t('Client JS Errors');
      foreach ($params['result'] as $record) {
        if (empty($record)) {
          continue;
        }
        $message['body'][] = check_plain($record->message);
      }
      $message['to'] = $params['to'];
      break;
  }
}

/**
 * Separates email addresses into an array.
 *
 * @param string $email_address
 *   Email address string.
 *
 * @return array
 *   Array of email address(es).
 */
function client_js_error_log_separate_email_address($email_address) {
  $email_addresses = array();
  $email_address = trim($email_address);
  if (!empty($email_address)) {
    $email_addresses
      = explode(CLIENT_JS_ERROR_LOG_EMAIL_ADDRESS_SEPARATOR, $email_address);
    $email_addresses = array_map(function ($email) {
      return trim($email);
    }, $email_addresses);
    $email_addresses = array_filter($email_addresses);
  }

  return $email_addresses;
}
