/**
 * @file
 * Client JS error handler function which catches and saves JS errors.
 */

/**
 * Closure for attach error logger function to window.onerror.
 *
 * It does not wait for document ready to catch errors also during
 * document is getting ready.
 * Instead of using Drupal.behaviors we extend Drupal object, because in this
 * case we can catch errors happen before document.ready
 * and can avoid global variables.
 */
(function () {
  'use strict';

  Drupal.clientJsErrorLog = function () {
    var log_max_count;
    var log_url;
    var js_error_count = 0;
    var original_onerror;

    var consoleLog = function () {

      /* eslint no-console:0 */

      if ('console' in window && 'log' in window.console) {
        console.log.apply(console, arguments);
      }
    };

    return {
      onError: function (msg, url, line) {
        if (!log_max_count || !log_url) {
          consoleLog(Drupal.t('@variable1 or @variable2 is not presented yet',
            {'@variable1': 'log_max_count', '@variable2': 'log_url'}));
          return false;
        }
        var column = arguments[3] || 0;
        var errorObj = arguments[4] || {};
        var stack = '';
        if (errorObj instanceof Error) {
          stack = errorObj.stack || errorObj.message || errorObj.toString();
        }
        ++js_error_count;

        if (js_error_count <= log_max_count) {
          if ('jQuery' in window && 'post' in jQuery) {
            setTimeout(function () {
              jQuery.post(log_url,
                {
                  error: {
                    message: msg,
                    url: url,
                    line: line,
                    column: column,
                    stack: stack
                  }
                });
            }, 500);
          }
          else {
            if (js_error_count === 1) {
              consoleLog(
                Drupal
                  .t('You have to install jQuery to use Client JS error log!')
              );
            }
          }
        }
        // Call the original handler if exists.
        if (original_onerror && typeof original_onerror === 'function') {
          original_onerror.apply(window, arguments);
        }
      },
      setLogMaxCount: function (count) {
        log_max_count = count;
        return this;
      },
      setLogUrl: function (url) {
        log_url = url;
        return this;
      },
      setOriginalOnError: function (onError) {
        original_onerror = onError;
      }
    };
  }();

  if ('onerror' in window) {
    Drupal.clientJsErrorLog.setOriginalOnError(window.onerror);
    window.onerror = Drupal.clientJsErrorLog.onError;
  }
})();
