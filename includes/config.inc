<?php

/**
 * @file
 * Configuration file for the Client js error log module.
 */

/**
 * URL of the AJAX logging.
 */
define('CLIENT_JS_ERROR_LOG_URL', 'save_client_js_error_log');


/**
 * Check count of log records from the last x minutes.
 */
define('DEFAULT_CLIENT_JS_ERROR_LOG_INTERVAL_MINUTE', 5);
define('MAXIMUM_CLIENT_JS_ERROR_LOG_INTERVAL_MINUTE', 100);

/**
 * How many request should be logged within x minutes.
 */
define('DEFAULT_CLIENT_JS_ERROR_LOG_MAX_IN_INTERVAL', 10);
define('MAXIMUM_CLIENT_JS_ERROR_LOG_MAX_IN_INTERVAL', 1000);

/**
 * How many error should be sent to the server to logging from the user browser.
 */
define('DEFAULT_CLIENT_JS_ERROR_LOG_MAX_COUNT', 3);
define('MAXIMUM_CLIENT_JS_ERROR_LOG_MAX_COUNT', 100);

/**
 * Delete those records from table which older than this days.
 */
define('DEFAULT_CLIENT_JS_ERROR_LOG_DELETE_RECORD_INTERVAL_DAY', 3);
define('MAXIMUM_CLIENT_JS_ERROR_LOG_DELETE_RECORD_INTERVAL_DAY', 100);

/**
 * The system sends the aggregated log records in email in every x hour.
 */
define('DEFAULT_CLIENT_JS_ERROR_LOG_SEND_EMAIL_INTERVAL_HOUR', 24);
define('MINIMUM_CLIENT_JS_ERROR_LOG_SEND_EMAIL_INTERVAL_HOUR', 1);
define('MAXIMUM_CLIENT_JS_ERROR_LOG_SEND_EMAIL_INTERVAL_HOUR', 1440);

/**
 * Name of the log table.
 */
define('TABLE_CLIENT_JS_ERROR_LOG', 'client_js_error_log');

/**
 * Email address separator character.
 */
define('CLIENT_JS_ERROR_LOG_EMAIL_ADDRESS_SEPARATOR', ',');
