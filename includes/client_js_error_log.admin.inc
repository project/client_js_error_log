<?php

/**
 * @file
 * Administrative page callbacks for the Client js error log module.
 */

/**
 * Callback function to generate the admin settings form.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 *
 * @return array
 *   The form structure.
 */
function client_js_error_log_admin_settings_form($form, &$form_state) {

  $form['client_js_error_log_max_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of log records to send from the client'),
    '#default_value' => variable_get('client_js_error_log_max_count',
      DEFAULT_CLIENT_JS_ERROR_LOG_MAX_COUNT),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Determines how many error instance will be logged from the client browser.'),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_number', 'element_validate_integer_positive'),
  );
  $form['client_js_error_log_interval_minute'] = array(
    '#type' => 'textfield',
    '#title' => t('Time interval to allow logging'),
    '#default_value' => variable_get('client_js_error_log_interval_minute',
      DEFAULT_CLIENT_JS_ERROR_LOG_INTERVAL_MINUTE),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('For example: If you set it to 10 the system will allow only x record to save within the last 10 minutes.'),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_number', 'element_validate_integer_positive'),
  );
  $form['client_js_error_log_max_in_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of log records within the allowed interval'),
    '#default_value' => variable_get('client_js_error_log_max_in_interval',
      DEFAULT_CLIENT_JS_ERROR_LOG_MAX_IN_INTERVAL),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('For example: If you set it to 30 the system will allow only 30 record to save within the last x minutes.'),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_number', 'element_validate_integer_positive'),
  );
  $form['client_js_error_log_delete_record_interval_day'] = array(
    '#type' => 'textfield',
    '#title' => t('Day interval to delete older records'),
    '#default_value' => variable_get('client_js_error_log_delete_record_interval_day',
      DEFAULT_CLIENT_JS_ERROR_LOG_DELETE_RECORD_INTERVAL_DAY),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('For example: If you set it to 7 the system will delete those records which are older than 7 days.'),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_number', 'element_validate_integer_positive'),
  );
  $form['client_js_error_log_mail_address'] = array(
    '#type' => 'textfield',
    '#title' => t('Email to'),
    '#default_value' => variable_get('client_js_error_log_mail_address', ''),
    '#description' => t('Email address(es) to send JS aggregated errors in email. Leave it empty to disable this feature. You can set more email separated by comma characters'),
  );
  $form['client_js_error_log_send_email_interval_hour'] = array(
    '#type' => 'textfield',
    '#title' => t('Send aggregated log record email after this hour.'),
    '#default_value' => variable_get('client_js_error_log_send_email_interval_hour',
      DEFAULT_CLIENT_JS_ERROR_LOG_SEND_EMAIL_INTERVAL_HOUR),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('For example: If you set it to 24 the system will send the summary email after 24 hours. (Only if you set email address).'),
    '#required' => TRUE,
    '#element_validate' => array('element_validate_number'),
  );

  return system_settings_form($form);
}

/**
 * Validates the settings.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function client_js_error_log_admin_settings_form_validate($form, &$form_state) {
  $log_max_count = $form_state['values']['client_js_error_log_max_count'];
  if ($log_max_count > MAXIMUM_CLIENT_JS_ERROR_LOG_MAX_COUNT) {
    form_set_error('client_js_error_log_max_count',
      t('Maximum number of log records must be @value.',
        array('@value' => MAXIMUM_CLIENT_JS_ERROR_LOG_MAX_COUNT)));
  }

  $interval_minute = $form_state['values']['client_js_error_log_interval_minute'];
  if ($interval_minute > MAXIMUM_CLIENT_JS_ERROR_LOG_INTERVAL_MINUTE) {
    form_set_error('client_js_error_log_interval_minute',
      t('Maximum value of interval minute must be @value.',
        array('@value' => MAXIMUM_CLIENT_JS_ERROR_LOG_INTERVAL_MINUTE)));
  }

  $max_in_interval
    = $form_state['values']['client_js_error_log_max_in_interval'];
  if ($max_in_interval > MAXIMUM_CLIENT_JS_ERROR_LOG_MAX_IN_INTERVAL) {
    form_set_error('client_js_error_log_max_in_interval',
      t('Maximum value of interval must be @value.',
        array('@value' => MAXIMUM_CLIENT_JS_ERROR_LOG_MAX_IN_INTERVAL)));
  }

  $record_interval_day
    = $form_state['values']['client_js_error_log_delete_record_interval_day'];
  if ($record_interval_day
    > MAXIMUM_CLIENT_JS_ERROR_LOG_DELETE_RECORD_INTERVAL_DAY
  ) {
    form_set_error('client_js_error_log_delete_record_interval_day',
      t('Maximum value of interval day must be @value.',
        array('@value' => MAXIMUM_CLIENT_JS_ERROR_LOG_DELETE_RECORD_INTERVAL_DAY)));
  }

  $email_interval_hour
    = $form_state['values']['client_js_error_log_send_email_interval_hour'];
  if ($email_interval_hour
    < MINIMUM_CLIENT_JS_ERROR_LOG_SEND_EMAIL_INTERVAL_HOUR
  ) {
    form_set_error('client_js_error_log_send_email_interval_hour',
      t('Minimum value of interval hour must be greater than @value.',
        array('@value' => MINIMUM_CLIENT_JS_ERROR_LOG_SEND_EMAIL_INTERVAL_HOUR)));
  }
  elseif ($email_interval_hour
    > MAXIMUM_CLIENT_JS_ERROR_LOG_SEND_EMAIL_INTERVAL_HOUR
  ) {
    form_set_error('client_js_error_log_send_email_interval_hour',
      t('Maximum value of interval hour must be @value.',
        array('@value' => MAXIMUM_CLIENT_JS_ERROR_LOG_SEND_EMAIL_INTERVAL_HOUR)));
  }

  $email_addresses = client_js_error_log_separate_email_address(
    $form_state['values']['client_js_error_log_mail_address']);

  foreach ($email_addresses as $email_address) {
    if (!valid_email_address($email_address)) {
      form_set_error('client_js_error_log_mail_address',
        t('You must enter email valid address (or more separated by @separator character).', array('@separator' => CLIENT_JS_ERROR_LOG_EMAIL_ADDRESS_SEPARATOR)));
      break;
    }
  }
}

/**
 * Shows log record lister page.
 *
 * @return array
 *   Array of data.
 *
 * @throws \Exception
 */
function client_js_error_log_overview() {

  $rows = array();

  $build['client_js_error_log_clear_log_form']
    = drupal_get_form('client_js_error_log_clear_log_form');

  $header = array(
    array('data' => t('Message'), 'field' => 'log.message'),
    array('data' => t('URL')),
    array('data' => t('User agent')),
    array('data' => t('Stack')),
    array('data' => t('User'), 'field' => 'u.name'),
    array('data' => t('IP address')),
    array(
      'data' => t('Create date'),
      'field' => 'log.create_date',
      'sort' => 'desc',
    ),
    array('data' => t('Referrer')),
    array('data' => t('Line')),
    array('data' => t('Column')),
  );

  $query = db_select(TABLE_CLIENT_JS_ERROR_LOG, 'log')
    ->extend('PagerDefault')
    ->extend('TableSort');
  $query->leftJoin('users', 'u', 'log.user_id = u.uid');
  $query
    ->fields('log', array(
      'id',
      'user_id',
      'message',
      'url',
      'line',
      'col_number',
      'stack',
      'referrer',
      'user_agent',
      'ip_address',
      'create_date',
    ))
    ->addField('u', 'name');

  $result = $query
    ->limit(50)
    ->orderByHeader($header)
    ->execute();

  foreach ($result as $record) {
    $rows[] = array(
      'data' => array(
        check_plain($record->message),
        filter_xss($record->url),
        filter_xss($record->user_agent),
        check_plain($record->stack),
        theme('username', array('account' => $record)),
        $record->ip_address,
        format_date(strtotime($record->create_date),
          'custom', 'm/d/Y - H:i:s'),
        filter_xss($record->referrer),
        check_plain($record->line),
        check_plain($record->col_number),
      ),
      'class' => array(drupal_html_class('client_js_error')),
    );
  }

  $build['client_js_error_log_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'client_js_error_log_table'),
    '#empty' => t('No log messages available.'),
  );
  $build['client_js_error_log_pager'] = array('#theme' => 'pager');

  return $build;
}

/**
 * Clears the log table.
 *
 * @param array $form
 *   Form array.
 *
 * @return array
 *   The form structure.
 */
function client_js_error_log_clear_log_form($form) {

  $form['client_js_error_log_clear'] = array(
    '#type' => 'fieldset',
    '#title' => t('Clear client js error log messages'),
    '#description' => t('This will permanently remove the client js error log messages from the database. You cannot undo.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['client_js_error_log_clear']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear log messages'),
    '#submit' => array('client_js_error_log_clear_log_submit'),
  );

  return $form;
}

/**
 * Form submission handler for client_js_error_log_clear_log_form().
 */
function client_js_error_log_clear_log_submit() {
  db_delete(TABLE_CLIENT_JS_ERROR_LOG)->execute();
  drupal_set_message(t('Client js error log cleared.'));
}
