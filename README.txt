CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

Client side JavaScript error logging module allows you to see errors
happen on client browser.

To discover problems on server side sometimes quite easy, just check log files
(e.g. Apache log)
but you do not really know what happen on client side that is like a black box.
You can get information based on user feedback if something wrong in the
front-end code.
This module helps you to keep in touch with your JavaScript code, you will
know the possible problems
arisen in the users browser. So you can find the problems more easy.

Features:

 * Flood control:
   you can configure how many error should be logged per user.

 * Aggregated log record summary email:
   You can be notified periodically if any JavaScript error happened on site,
   just configure the
   email notification and you will get an aggregated list of last errors.

 * Auto delete old records:
   You can set an interval to delete older records, so your database always
   will be in optimal size.


REQUIREMENTS
------------

 * It needs only jQuery library.

RECOMMENDED MODULES
-------------------

 None

INSTALLATION
------------

 * The installation so easy, just copy the code into your client_js_error_log
   folder in your module directory
   (it is most often the "site/all/modules") then enable the module.
   See: https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Enable the module.

 * Configure the settings

   Give the proper permission to roles at the admin/people/permissions section.
   You can allow the next two permissions:
   - Administer Client JS error log (usually for trusted persons)
   - View Client JS error log (for log viewer role)

   Then you can configure the module at:
   "Configuration/System/Client side JS error logging"
   (/admin/config/system/client-js-error)


 * Usage

   When you installed and enabled the module it will automatically log the
   client side JavaScript errors.
   You can see the saved records under the Reports/Client js error log
   messages menu item.
   To change the configuration you can go to the
   Configuration/System/Client side JS error logging

CONFIGURATION
-------------

On the configuration page you can set the next parameters:

 * "Maximum number of log records to send from the client"
   It determines how many error instance should be sent from the user browser.
   Example: if you set it to 3 and if in the user's browser happen 5 JavaScript
   error during a page view the system will
   send only 3 log report to the server for logging.

 * "Time interval to allow logging"
   To determine an interval for check how many error has been logged within this
   time period from the same IP address.
   For example: If you set it to 10 the system will allow only x record to save
   within the last 10 minutes.
   (x = "Maximum number of log records within the allowed interval")

 * "Maximum number of log records within the allowed interval"
   To determine an amount to check how many error has been logged within the
   "Time interval to allow logging" period
   from the same IP address.
   For example: If you set it to 30 the system will allow only 30 record to
   save within the last y minutes,
   more errors will be skipped.
   (y = "Time interval to allow logging")

 * "Day interval to delete older records"
   Interval in days for delete the older records from database.
   For example: If you set it to 7 the system will delete those records which
   are older than 7 days.

 * "Email to"
   You can set here the recipients email address(es) who will get email
   notification with the aggregated log records.
   To disable this feature just leave it empty.
   To use multiple recipients you can set email addresses separated by
   comma character.
   (for example: email1@example.com,email2@example.com,email3@example.com)

 * "Send aggregated log record email after this hour."
   The system will send periodically the summary email in every x hour.
   For instance if if you set it to 24 then the system will send the
   notification email after every 24 hour (daily).
   The summary email will contain the records only from the last x hour but
   maximum 50 to avoid email overload.

TROUBLESHOOTING
---------------

 * If you don't get email notification please check you have SMTP configured
 and email sending enabled on your site.

FAQ
---

Q: I don't have any questions.

A: Great!

MAINTAINERS
-----------

Current maintainers:

 * bazaalt.organ - https://www.drupal.org/project/user/3411937
